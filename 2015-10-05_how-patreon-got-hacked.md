# @WiseQuote: How Patreon got hacked

Remeber to disable debugging tools on your live server as they can be a
potential security risk. Patreon had to learn this the hard way. This is what
detectify labs had to say about how the hack was executed:

> Yesterday Patreon, which is a funding platform for artists and creators, went
> out with a Security Notice about a compromise happening on the 28th of
> September on one of their debug versions which was publicly available.
> Shortly after that, data from this instance, which contained live data, was
> publicly posted which you can read about here and here.
> 
> Their debug version of the application was running with the Werkzeug Debugger
> publicly available [..]
