# @Thoughts: My web dev toolbox

## Operating System stuff
* Ubuntu Mate as my dev machine host
* Several virtual machines:
	* Android x86
	* Mac OS X / XCode iPhone Simulator
	* Windows 7 with IE9
	* Windows XP with IE8
	* Vagrant Scotch Box (ubuntu server with apache webserver)
	* Vagrant VVV (ubuntu server with nginx webserver)

## Other software I use daily
* Bower to easily manage my dependencies
* Git because I need to keep track of things, and git is great at doing that
* Gulp to automate stuff like minification and watch / refresh cycles
* Tilda because it's the best drop-down terminal there is
* Tmux because I like it better than screen and it feels like a tiling WM
* Vagrant for quickly spinning up pre-configured virtual environments
* Vim for all my coding needs, remote and local
* Virtual Box as the basis for my Vagrant setup (see above)
* Weinre for cross-device testing and debugging

## My development hardware
* Thinkpad X230 as my main development machine
* Various brands of smartphones and tablets for on-device testing
