# @Todo: Domainkeys and SPF

Just a quick reminder: If you want to avoid having your e-mail classified as
potential spam by GMail then make sure you use a recent email-validation sytem
like SPF (Sender Policy Framework) and _not_ something outdated like DK
(DomainKeys).
