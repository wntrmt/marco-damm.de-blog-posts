# Be a good manager

Communication and trust are the two cornerstones
of a really solid manager <-> managee
relationship in my experience. Whether that lack
of trust manifests itself as micro-management,
constant check-ins, or a constant threat of
surveillance it can easily turn an above average
performer into an apathetic and demoralized
employee.

eople who are given the space to make choices,
be creative, and fail every so often, will often
figure out how to manage themselves
