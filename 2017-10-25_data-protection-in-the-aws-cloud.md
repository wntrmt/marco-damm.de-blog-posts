# @Thoughts: Data Protection in the AWS Cloud Implementing GDPR and Overview of C5

## What is C5?
C5 (Cloud Computing Compliance Controls Catalogue) is designed by the BSI in Feb. 2016.

International standards taken into account for C5:

* ISO/IEC 27001:2013
* CSA Cloud Controls Matrix 3.01
* AICPA Trust Service Principles Criteria 2014
* ANSSI Referentiel Secure Cloud 2.0
* and more

C5 makes cloud service providers comparable by a set of features.

## Basics on GDPR
GDPR will come into effect on 25.05.2018 and follows EU Data Protection
Directive 95/46/EC. Because of coming changes it makes sense to start looking
into **now**.

The EU recommendation is replaced by a harmonized law for all EU member states.

* It applies to all companies processing and holding the personal data of data
  subjects residing in the EU, regardless of company's location

* In Germany up until now highest penalty was 300.000 Euros and will now go up
  to a maximum of 20.000.000 Euro penalties

* PII data transfers to countries outside the EU is now facing high hurdles.
  This data **does not** to be stored outside EU to count as transfer

* All persons now have a right to data portability. Every person can now
  request stored data in a portable manner

* Right to be forgotten. Data must be deleted on request

* Privacy by design. Software has to be developed with privacy in mind

* Authorities have to be notified within 72 hours if there is a breach

For more information have a look at article 28 of new law.

## Data protection is a shared responsibility
* Customer is data controller
* AWS is the data (sub) processor

![Shared data protection responsibility](/marco-damm.de/images/data-protection-shared-responsibility.jpg)

Amazon is responsible for recertify its services with various agencies. Amazon
provides compliance best practices. Implementation of data protection  on an
application, operating system, network level has to be provided by the
customer.

## AWS Data Processing Agreement
AWS offers GDPR-ready DPA available to customers **today**. If applicable, any
existing DPAs become invalid on the night of the 25th of May 2018.

## Navigting GDPR Compliance on AWS
![Navigating GDPR Compliance on AWS 01](/marco-damm.de/images/navigating-gdpr-compliance-01.jpg)
![Navigating GDPR Compliance on AWS 02](/marco-damm.de/images/navigating-gdpr-compliance-02.jpg)
![Navigating GDPR Compliance on AWS 03](/marco-damm.de/images/navigating-gdpr-compliance-03.jpg)

— <cite>Gerald Boyne, Christian Hesse (Security Assurance AWS Germany)</cite>
