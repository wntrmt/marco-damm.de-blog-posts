## Component Tree / Component Structure Planning

* Split up web design to React components

## Application State (Data) Planning

* Plan which state to use in the application

## Components vs. Containers Planning

* Answer which components should be stateless and which components should be stateful
