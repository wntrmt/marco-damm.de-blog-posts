# @Thoughts: tmux, tilda and vim

I have been using sublime text (ST) at work for quite a while now and I really
love stuff like multi-cursor editing or the mini-map navigation. Being a GUI
software, ST is intended for use on a local workstation. Since I had (and still
have) no idea if you can somehow edit remote files with ST, I decided to give
vim a shot.

## What is Vim?
To quote Wikipedia:
> Vim is a clone of Bill Joy's vi editor for Unix. It was written by Bram
> Moolenaar based on source for a port of the Stevie editor to the Amiga and
> first released publicly in 1991. Vim is designed for use both from a
> command-line interface and as a standalone application in a graphical user
> interface. Vim is free and open source software and is released under a
> license that includes some charityware clauses, encouraging users who enjoy
> the software to consider donating to children in Uganda. The license is
> compatible with the GNU General Public License.

## So what is tmux?
Tmux is a *t*erminal *mu*ltiple*x*er, and its purpose is to enable you to use
any number of terminals within a terminal. If you know screen you get the
general idea, otherwise take a look at the screenshot below:

![Screenshot of Tmux in action](assets/images/posts/vim-tmux-tilda.png "Tmux with 4 panes running cmatrix and vim")

A practical application for web development with tmux would be having three
terminal sessions in a grid layout like the one above. The left most sessions
containing your code editor (i.e. vim), the session in the upper right
containing the output of some monitoring process like browser-sync or grunt,
and finally the session in the lower right for committing and pushing any
changes made to your code.

## I know what a tilde is, but what is tilda?
Have you ever played Quake or a similar 1st person shooter where you could
access a console for entering commands by pressing the ~ key? Well, tilda does
that for your Linux desktop! Press ~ and a drop-down console will appear from
the top of your screen. Enter commands, execute scripts or start programs just
like you would do with any command line interface. Press ~ again and make your
drop-down console disappear again!
