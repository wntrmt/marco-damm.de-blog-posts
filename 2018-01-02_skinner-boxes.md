# Skinner Boxes


Examples of skinner boxes are everywhere in society today. They've been around longer than they were identified by Dr. Skinner. Anything that gives a variable reward for pressing the same button is participating in a sort of skinner box.

Mobile games use them, slot machines use them, and you can even think of facebook/instagram likes as such variable rewards you get for the same action over and over.
They are pervasive in society already.