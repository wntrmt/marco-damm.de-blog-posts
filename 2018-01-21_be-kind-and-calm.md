# Being kinder and calmer


1. Work on your health. Being in better physical
shape makes it easier to take such things in
stride.

2. Look to your health in the moment as well.
While raising special needs kids, I learned that
a glass of water, something to eat and/or a
short nap was often the difference between
losing my shit and continuing to rise to the
occasion. So, stop, go to the break room and
take care of yourself for 5 minutes. Then deal
with it.

3. Journal and work on the person in the mirror.
Often, things get a rise out of us more because
of baggage from the past than because of how bad
it really is right now.

4. Do volunteer work in something that exposes
you to problems that get a big reaction out of
you. Having perspective can be very valuable for
keeping your cool in the face of stressful work
situations. If you deal with life threatening,
messy problems on the weekend as a volunteer,
most problems that come up in an office job just
won't get past your callouses enough to get
under your skin.

5. Educate yourself on how to effectively solve
various problems. Knowing you can fix it is
probably the single biggest source of calm in
the face of a tempest. So up your game. Figure
out what your weak areas are and start filling
them in.

