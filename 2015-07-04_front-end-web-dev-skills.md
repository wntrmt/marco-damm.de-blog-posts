# @Thoughts: Web Dev skills

[Laurence Bradford](http://learntocodewith.me/web-dev/front-end-developer-skills/) compiled a nice list of must-have front-end web developer skills from various ressources including job listings and front-end developer courses. Here is a list of skills she recommends:

## ESSENTIAL FRONT END SKILLS  
- HTML &#x2713;
- CSS &#x2713;
- JavaScript/jQuery &#x2713;

## CSS FRAMEWORKS (learn one to start)
- Bootstrap &#x2713;
- Foundation 

## CSS TOOLS
- Responsive design principles 
- Preprocessors (learn one to start)
  - Sass
  - Less
  - Stylus

## JAVASCRIPT FRAMEWORKS (learn one to start)
- Angular.js
- Backbone.js
- React.js

## NICE-TO-HAVES
- Git/Github know-how &#x2713;
- Unit testing
  - Jasmine
  - Karma
- Package management: Bower &#x2713;
- Task runners
  - Grunt
  - Gulp
- JavaScript module loaders
  - Require.js
  - Browserify

The front-end developer's counterpart is the back-end developer whose skills I will cover in my next post. What you will most probably also find in many smaller companies and especially start-ups are full-stack developers. Full-stack development combines both front-end and back-end development into one (code-god-like) person.
